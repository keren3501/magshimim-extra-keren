#!/usr/bin/python

from __future__ import print_function
from bcc import ArgString, BPF
from bcc.utils import printb
import argparse
import ctypes as ct
from datetime import datetime, timedelta
import os
import iptc
import socket
import struct
import ipaddress

# define BPF program
bpf_text = """
#include <linux/ip.h>
#include <linux/icmp.h>
#include <linux/types.h>
#include <uapi/linux/ptrace.h>
#include <uapi/linux/limits.h>
#include <linux/sched.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/inet.h>

struct val_t {
    u64 id;
    char comm[TASK_COMM_LEN];
	struct user_msghdr* message;
};

struct data_t {
    u64 id;
    u64 ts;
	u32 ip;
};

BPF_HASH(infotmp, u64, struct val_t);
BPF_PERF_OUTPUT(events);

int trace_entry(struct pt_regs *ctx, int sockfd, struct user_msghdr *msg, int flags)
{
	struct val_t val = {};
    u64 id = bpf_get_current_pid_tgid();

    if (bpf_get_current_comm(&val.comm, sizeof(val.comm)) == 0) {
        val.id = id;
		val.message = msg;
		infotmp.update(&id, &val);		
	}
    return 0;
};

int trace_return(struct pt_regs *ctx)
{
	u64 id = bpf_get_current_pid_tgid();
    struct val_t *valp;
    struct data_t data = {};

    valp = infotmp.lookup(&id);
    if (valp == 0) {
        // missed entry
        return 0;
    }

    data.id = valp->id;
    data.ts = bpf_ktime_get_ns();
	struct user_msghdr *message = valp->message;

	__u8 *buf = message->msg_iov->iov_base;
	struct iphdr *ip = (struct iphdr *)message->msg_iov->iov_base;

	/* Check the IP header */
	//struct iphdr *ip = (struct iphdr *)buf;
	int hlen = ((__u8)ip >> 4) * 4;
	data.ip = ip->saddr;

	/* Check the ICMP header */
	struct icmphdr *icp = (struct icmphdr *)(buf + (hlen));
	if (icp->type == 8) 
	{
	    events.perf_submit(ctx, &data, sizeof(data));
	}

    infotmp.delete(&id);
    return 0;
}
"""

# initialize BPF
b = BPF(text=bpf_text)
event_name = b.get_syscall_fnname("recvmsg")
b.attach_kprobe(event=event_name, fn_name="trace_entry")
b.attach_kretprobe(event=event_name, fn_name="trace_return")

TASK_COMM_LEN = 16    # linux/sched.h
NAME_MAX = 255        # linux/limits.h
ADDRESS_LEN = 16

class Data(ct.Structure):
    _fields_ = [
        ("id", ct.c_ulonglong),
        ("ts", ct.c_ulonglong),
		("ip", ct.c_uint),
	]

def int2ip(addr):
    return (socket.inet_ntoa(struct.pack("<L", addr)))


# header
print("recvmsg snoop")

pings = []

def stop_attack(ping):
	for packet in ping:
		ip = int2ip(packet.ip)
		print("DROP " + ip)
		chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "INPUT")
		rule = iptc.Rule()
		rule.src = ip
		target = iptc.Target(rule, "DROP")
		rule.target = target
		chain.insert_rule(rule)

# process event
def print_event(cpu, data, size):
	event = ct.cast(data, ct.POINTER(Data)).contents
	pings.append([event])
	for i in range(0, len(pings)-1)[::-1]:
		if event.ts <= pings[i][0].ts + 1000000000:
			pings[i].append(event)
		if len(pings[i]) > 20:
			#attack!
			print ("-----------------")
			stop_attack(pings[i])
			del pings[i]


# loop with callback to print_event
b["events"].open_perf_buffer(print_event)
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
